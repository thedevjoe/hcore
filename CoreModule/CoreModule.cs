﻿using System;
using System.Threading.Tasks;
using Discord.WebSocket;
using HCore.Attributes;

namespace CoreModule
{
    public class CoreModule
    {
        [HCEntryPoint]
        public static void Init(HCore.HCore core)
        {
            core.RegisterDiscordCommands(typeof(CoreModule));
        }

        [DiscordCommandInfo("test", "Does a test thing")]
        [CommandParameter("wow", false)]
        public static async Task TestCommand(SocketMessage message, string[] args)
        {
            await message.Channel.SendMessageAsync("Test");
        }
    }
}