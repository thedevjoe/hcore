﻿using System;
using System.Threading.Tasks;

namespace HCore
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("=======================================");
            Console.WriteLine("HCore by devjoe.net");
            Console.WriteLine("=======================================");
            Console.WriteLine("Initializing components...");
            
            DotNetEnv.Env.Load();
            HCore hc = new HCore();
            hc.Initialize().GetAwaiter().GetResult();
            
            Task.Delay(-1).GetAwaiter().GetResult();
        }
    }
}