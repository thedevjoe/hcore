using System;
using System.Collections.Generic;
using Discord.WebSocket;
using HCore.Commands;

namespace HCore.Managers
{
    public class DiscordCommandManager
    {
        private static DiscordCommandManager _instance;
        internal List<DiscordCommand> Commands = new List<DiscordCommand>();
        public string Prefix { get; private set; }

        internal DiscordCommandManager()
        {
            _instance = this;
            Prefix = System.Environment.GetEnvironmentVariable("BOT_PREFIX") ?? "!";
        }

        public static DiscordCommandManager GetInstance()
        {
            if(_instance == null) return new DiscordCommandManager();
            return _instance;
        }

        /// <summary>
        /// Gets a Discord command by name
        /// </summary>
        /// <param name="name">The name of the Discord command</param>
        /// <returns>A command or NULL if the command does not exist.</returns>
        public DiscordCommand GetCommandByName(string name)
        {
            foreach (DiscordCommand c in Commands)
            {
                if (c.CommandInfo.Name.ToLower().Equals(name)) return c;
            }

            return null;
        }

        /// <summary>
        /// Gets all registered commands
        /// </summary>
        /// <returns>An array of commands</returns>
        public DiscordCommand[] GetAllCommands()
        {
            return Commands.ToArray();
        }

        public void ExecuteCommand(SocketMessage message)
        {
            DiscordCommandExecution exe = GetExecution(message.Content);
            if (exe == null) return;
            exe.Message = message;
            exe.Action.Invoke();
        }

        public DiscordCommandExecution GetExecution(string message)
        {
            if (!message.StartsWith(Prefix)) return null;
            message = message.Substring(Prefix.Length);
            string[] spl = message.Split(' ');
            string[] args = new string[spl.Length - 1];
            Array.Copy(spl, 1, args, 0, spl.Length-1);
            string command = spl[0];
            DiscordCommand cmd = GetCommandByName(command);
            if (cmd == null) return null;
            var v = new DiscordCommandExecution()
            {
                Method = cmd,
                Parameters = args
            };
            v.Action = () =>
            {
                cmd.Method.Invoke(null, new object[] {v.Message, v.Parameters});
            };
            return v;
        }
    }
}