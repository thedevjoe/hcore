using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using HCore.Attributes;
using HCore.Commands;
using HCore.Managers;

namespace HCore
{
    public class HCore
    {
        private static HCore _instance;
        public DiscordShardedClient Client;
        internal HCore()
        {
            _instance = this;
        }

        internal async Task Initialize()
        {
            Client = new DiscordShardedClient();
            Client.MessageReceived += ClientOnMessageReceived;
            Client.LoggedIn += ClientOnLoggedIn;
            Client.LoggedOut += ClientOnLoggedOut;
            Client.ShardReady += ClientOnShardReady;
            Client.Log += ClientOnLog;
            await Client.LoginAsync(TokenType.Bot, System.Environment.GetEnvironmentVariable("DISCORD_BOT_TOKEN"));
            await Client.StartAsync();
            if (!Directory.Exists("modules")) Directory.CreateDirectory("modules");
            foreach(string f in Directory.EnumerateFiles("modules"))
            {
                Assembly s = Assembly.LoadFile(Path.GetFullPath(f));
                Console.WriteLine("Discovered assembly " + s);
                foreach (Type t in s.GetTypes())
                {
                    MethodInfo method = t.GetMethods().FirstOrDefault(e => e.GetCustomAttribute(typeof(HCEntryPoint)) != null);
                    if (method == null) continue;
                    method.Invoke(null, new object[]{this});
                    
                }
            }
            Console.WriteLine("Ready to roll.");
        }

        private async Task ClientOnLog(LogMessage arg)
        {
            Console.WriteLine("[DDN] " + arg.Message);
        }

        private async Task ClientOnShardReady(DiscordSocketClient arg)
        {
            Console.WriteLine("Discord is ready.");
        }

        private async Task ClientOnLoggedOut()
        {
            Console.WriteLine("Disconnected.");
        }

        private async Task ClientOnLoggedIn()
        {
            Console.WriteLine("Logged into Discord.");
            
        }

        private async Task ClientOnMessageReceived(SocketMessage arg)
        {
            DiscordCommandManager.GetInstance().ExecuteCommand(arg);
        }

        internal void RegisterCLICommands()
        {
            
        }

        public void RegisterDiscordCommands(Type type)
        {
            foreach (var method in type.GetMethods())
            {
                DiscordCommandInfo info = method.GetCustomAttribute<DiscordCommandInfo>();
                if (info == null) continue;
                ParameterInfo[] inf = method.GetParameters();
                if (inf.Length != 2)
                {
                    Console.WriteLine("Refusing to register command " + info.Name + " from " + type.Assembly + ": Invalid Parameter Length");
                    continue;
                }

                if (inf[0].ParameterType != typeof(SocketMessage) ||
                    inf[1].ParameterType != typeof(string[]))
                {
                    Console.WriteLine("Refusing to register command " + info.Name + " from " + type.Assembly + ": Invalid Parameters");
                    continue;
                }
                if (method.ReturnParameter == null || method.ReturnParameter.ParameterType != typeof(Task))
                {
                    Console.WriteLine("Refusing to register command " + info.Name + " from " + type.Assembly + ": Invalid Return Type");
                    continue;
                }
                CommandParameter[] pms = (CommandParameter[])method.GetCustomAttributes(typeof(CommandParameter));
                DiscordCommand cmd = new DiscordCommand();
                cmd.CommandInfo = info;
                cmd.Parameters = pms;
                cmd.Method = method;
                DiscordCommandManager.GetInstance().Commands.Add(cmd);
                Console.WriteLine("Registered command " + cmd.CommandInfo.Name + " from " + type.Assembly);
            }
        }
    }
}