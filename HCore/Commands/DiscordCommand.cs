using System.Reflection;
using HCore.Attributes;

namespace HCore.Commands
{
    public class DiscordCommand
    {
        public DiscordCommandInfo CommandInfo;
        public CommandParameter[] Parameters;
        public MethodInfo Method;
    }
}