using System;
using System.Reflection;
using Discord.WebSocket;

namespace HCore.Commands
{
    public class DiscordCommandExecution
    {
        public DiscordCommand Method;
        public SocketMessage Message;
        public string[] Parameters;
        public Action Action;
    }
}