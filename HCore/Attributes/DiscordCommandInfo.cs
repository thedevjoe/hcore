using System;

namespace HCore.Attributes
{
    public class DiscordCommandInfo : Attribute
    {
        public string Name { get; }
        public string Description { get; }
        
        
        /// <summary>
        /// Specifies command information
        /// </summary>
        /// <param name="name">The name of the command</param>
        /// <param name="description">A short description</param>
        public DiscordCommandInfo(string name, string description)
        {
            this.Name = name;
            this.Description = description;
        }
    }
}