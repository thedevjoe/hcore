using System;

namespace HCore.Attributes
{
    /// <summary>
    /// The entry point of the dynamically loaded module
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class HCEntryPoint : Attribute
    {
        
    }
}