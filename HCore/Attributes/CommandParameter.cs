using System;

namespace HCore.Attributes
{
    public class CommandParameter : Attribute
    {
        public string Name { get; }
        public bool Required { get; }
        
        /// <summary>
        /// Specifies a command parameter
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="required">Is the parameter required?</param>
        public CommandParameter(string name, bool required = true)
        {
            this.Name = name;
            this.Required = required;
        }
    }
}